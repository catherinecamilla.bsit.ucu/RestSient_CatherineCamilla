import 'package:flutter/material.dart';
import 'package:restsient_catherine/service_page.dart';

class Dashboard_page extends StatelessWidget {
  const Dashboard_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(137, 237, 236, 1),
      body: Center(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              
              child: Image.asset(
                'assets/house.png',
                fit: BoxFit.cover,
              ),
            ),
            Spacer(),
            Text(
              "Welcome to RestSient",
              style: TextStyle(
                  fontSize: 25,
                  color: Color.fromRGBO(65, 0, 0, 5).withOpacity(.6),
                  fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 8,
                  child: ElevatedButton.icon(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Service_page(),
                          ));
                    },
                    icon: Icon(
                      Icons.house_outlined,
                      size: 90,
                    ),
                    label: Text(
                      "Available House",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Color.fromARGB(255, 24, 108, 108),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Spacer(),
              ],
            ),
            Spacer(),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 8,
                  child: ElevatedButton.icon(
                    onPressed: () {},
                    icon: Icon(
                      Icons.contact_phone_outlined,
                      size: 80,
                    ),
                    label: Text(
                      "Contact Owner",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Color.fromARGB(255, 24, 108, 108),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Spacer(),
              ],
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
