import 'package:flutter/material.dart';
import 'package:restsient_catherine/login_page.dart';

class Start_page extends StatelessWidget {
  const Start_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(137, 237, 236, 1),
      body: Center(
        child: Column(
          children: [
            Spacer(),
            Text(
              "RestSient",
              style: TextStyle(
                  fontSize: 35,
                  color: Color.fromRGBO(65, 0, 0, 5).withOpacity(.6),
                  fontWeight: FontWeight.bold),
            ),
            Expanded(
              flex: 2,
              child: Image.asset('assets/logo.png'),
            ),
            Expanded(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 24, 108, 108),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Spacer(),
                    Row(
                      children: [
                        Spacer(),
                        Text(
                          "Good Day!",
                          style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                        Spacer(),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Spacer(),
                        Text(
                          "Transient house located in",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                        Spacer(),
                      ],
                    ),
                    Row(
                      children: [
                        Spacer(),
                        Text(
                          "Urdaneta City, Camantilles",
                          style: TextStyle(fontSize: 15, color: Colors.white),
                        ),
                        Spacer(),
                      ],
                    ),
                    Spacer(),
                    Row(
                      children: [
                        Spacer(),
                        ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Login_page(),
                                ));
                          },
                          child: Text(
                            "Get Started",
                            style: TextStyle(color: Colors.black),
                          ),
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Color.fromARGB(255, 163, 231, 210),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            padding: EdgeInsets.all(20),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
