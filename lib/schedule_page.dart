import 'package:flutter/material.dart';
import 'package:restsient_catherine/confirm_page.dart';
import 'package:restsient_catherine/start_page.dart';

class Schedule_page extends StatefulWidget {
  const Schedule_page({super.key});

  @override
  State<Schedule_page> createState() => _Schedule_pageState();
}

class _Schedule_pageState extends State<Schedule_page> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(137, 237, 236, 1),
      body: Center(
        child: Column(
          children: [
            Spacer(),
            Text(
              "Your choice is Transient",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Text(
              "house #1",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Image.asset('assets/house1.png'),
            Spacer(),
            Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 8,
                  child: Container(
                    padding: EdgeInsets.only(top: 5, bottom: 10),
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 24, 108, 108),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Center(
                      child: Text(
                        "Lend your Information",
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
                Spacer(),
              ],
            ),
            Expanded(
                flex: 10,
                child: Padding(
                  padding: const EdgeInsets.all(14),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: Color.fromARGB(255, 24, 108, 108),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Spacer(),
                          TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Color.fromRGBO(219, 252, 252, 1),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              hintText: "Name",
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Please enter Name";
                              }
                              return null;
                            },
                          ),
                          Spacer(),
                          TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Color.fromRGBO(219, 252, 252, 1),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              hintText: "Date of Check in",
                              suffixIcon: Icon(Icons.calendar_today_outlined),
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Please enter Date";
                              }
                              return null;
                            },
                          ),
                          Spacer(),
                          TextFormField(
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Color.fromRGBO(219, 252, 252, 1),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(50)),
                              hintText: "Time of Check in",
                              suffixIcon: Icon(Icons.access_time),
                            ),
                            validator: (String? value) {
                              if (value == null || value.isEmpty) {
                                return "Please enter Time";
                              }
                              return null;
                            },
                          ),
                          Spacer(),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              padding: EdgeInsets.all(15),
                              backgroundColor: Color.fromRGBO(137, 237, 236, 1),
                            ),
                            onPressed: () {
                              if (_formKey.currentState!.validate()) {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          const Confirm_page(),
                                    ));
                              }
                            },
                            child: Text(
                              "SUBMIT",
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
