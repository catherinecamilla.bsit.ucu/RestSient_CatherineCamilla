import 'package:flutter/material.dart';
import 'package:restsient_catherine/schedule_page.dart';

class Service_page extends StatelessWidget {
  const Service_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(137, 237, 236, 1),
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                Spacer(),
                Expanded(
                    flex: 8,
                    child: Container(
                      padding: EdgeInsets.only(top: 5, bottom: 10),
                      decoration: BoxDecoration(
                        color: Color.fromARGB(255, 24, 108, 108),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                          "Available House",
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    )),
                Spacer(),
              ],
            ),
            Expanded(
              flex: 8,
              child: Container(
                child: ListView(
                  children: [
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 600,
                      child: Column(
                        children: [
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "Transient house #1",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Spacer(),
                          Image.asset('assets/house1.png'),
                          Spacer(),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Spacer(),
                              Icon(
                                Icons.location_on_outlined,
                                size: 30,
                              ),
                              Spacer(),
                              Text(
                                "#189 Zone 6, Camantilles Urdaneta\nCity Pangasinan",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "Entaire house\n (Room size: 430 m²/4628 ft²)",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "4 bedrooms",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Spacer(),
                              Icon(
                                Icons.house_outlined,
                                size: 30,
                              ),
                              Spacer(),
                              Text(
                                " Bedroom #1 - 1 king size bed\n Bedroom #2 - 1 king size bed\n Bedroom #3 - 2 queen size bed\n Bedroom #4 - 2 queen size bed",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "4 bathrooms\n Kitchen - Cook for yourself",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Schedule_page(),
                                ),
                              );
                            },
                            child: Text(
                              "Check In",
                              style: TextStyle(fontSize: 18),
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  Color.fromARGB(255, 24, 108, 108),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 600,
                      child: Column(
                        children: [
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "Transient house #2",
                                style: TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          Spacer(),
                          Image.asset('assets/house2.png'),
                          Spacer(),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Spacer(),
                              Icon(
                                Icons.location_on_outlined,
                                size: 30,
                              ),
                              Spacer(),
                              Text(
                                "#189 Zone 6, Camantilles Urdaneta\nCity Pangasinan",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "Entaire house\n (Room size: 430 m²/4628 ft²)",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "3 bedrooms",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Spacer(),
                              Icon(
                                Icons.house_outlined,
                                size: 30,
                              ),
                              Spacer(),
                              Text(
                                " Bedroom #1 - 1 king size bed\n Bedroom #2 - 1 king size bed\n Bedroom #3 - 2 queen size bed",
                                style: TextStyle(fontSize: 18),
                              ),
                              Spacer(),
                            ],
                          ),
                          Spacer(),
                          Row(
                            children: [
                              Text(
                                "4 bathrooms\n Kitchen - Cook for yourself",
                                style: TextStyle(fontSize: 18),
                              ),
                            ],
                          ),
                          Spacer(),
                          ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              "Check In",
                              style: TextStyle(fontSize: 18),
                            ),
                            style: ElevatedButton.styleFrom(
                              backgroundColor:
                                  Color.fromARGB(255, 24, 108, 108),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
