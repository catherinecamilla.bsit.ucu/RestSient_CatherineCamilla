import 'package:flutter/material.dart';
import 'package:restsient_catherine/dashboard_page.dart';

class Confirm_page extends StatelessWidget {
  const Confirm_page({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(137, 237, 236, 1),
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Dashboard_page(),
                          ));
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      size: 35,
                    ))
              ],
            ),
            Spacer(),
            Text(
              "Your Information",
              style: TextStyle(
                  fontSize: 30,
                  color: Color.fromRGBO(65, 0, 0, 5).withOpacity(.6),
                  fontWeight: FontWeight.bold),
            ),
            Text(
              "has been recorded",
              style: TextStyle(
                  fontSize: 30,
                  color: Color.fromRGBO(65, 0, 0, 5).withOpacity(.6),
                  fontWeight: FontWeight.bold),
            ),
            Spacer(),
            Icon(
              Icons.check_circle_outline_outlined,
              size: 120,
            ),
            Expanded(
              flex: 4,
              child: Container(
                padding: EdgeInsets.only(top: 30, bottom: 30),
                child: Column(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Image.asset(
                        'assets/logo.png',
                      ),
                    ),
                    Text(
                      "Thank you for choosing",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "RestSient! We're glad to provide",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "a peacful rest space for you",
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
