import 'package:flutter/material.dart';
import 'package:restsient_catherine/confirm_page.dart';
import 'package:restsient_catherine/dashboard_page.dart';
import 'package:restsient_catherine/login_page.dart';
import 'package:restsient_catherine/schedule_page.dart';
import 'package:restsient_catherine/service_page.dart';
import 'package:restsient_catherine/start_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Start_page(),
    );
  }
}
